package com.eugenew.assistantimport.assistant_import;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.ibm.cloud.sdk.core.security.IamAuthenticator;
import com.ibm.watson.assistant.v1.Assistant;
import com.ibm.watson.assistant.v1.model.CreateDialogNodeOptions;
import com.ibm.watson.assistant.v1.model.CreateIntentOptions;
import com.ibm.watson.assistant.v1.model.DialogNode;
import com.ibm.watson.assistant.v1.model.DialogNodeCollection;
import com.ibm.watson.assistant.v1.model.DialogNodeOutput;
import com.ibm.watson.assistant.v1.model.DialogNodeOutputGeneric;
import com.ibm.watson.assistant.v1.model.DialogNodeOutputTextValuesElement;
import com.ibm.watson.assistant.v1.model.Example;
import com.ibm.watson.assistant.v1.model.Intent;
import com.ibm.watson.assistant.v1.model.ListDialogNodesOptions;
import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvValidationException;

/**
 * Hello world!
 *
 */
public class App 
{
	// this set of credential is from eugene@chatbo.ai
	// for testing purpose only
	
	private static String workspaceId;// = "177e27b8-0924-46c7-842b-64cb2593e563";
	private static String apiKey;// = "gLVVXO_BA5whxEGboALB9UO9OBvXnUi5nvLiq6yYSI5P";
	private static String serviceUrl;// = "https://gateway.watsonplatform.net/assistant/api";
	private static String folderNodeId;// = "node_1_1576521463454";
	private static String csvName = "intent_import.csv";
	
	
	// set of credential is from chatbo@chatbo.ai - AML FAQ skill
	/*
	private static String workspaceId = "3ffd1467-b179-4032-8b82-6988bdd7bff6";
	private static String apiKey = "JlTNGJEpsRu7alfhyxJEchnXKmSa8GxY9bJP7z3Foofc";
	private static String serviceUrl = "https://gateway-wdc.watsonplatform.net/assistant/api";
	private static String folderNodeId = "node_2_1574790149306";
	private static String csvName = "intent_import.csv";
	*/
	
	public static void main( String[] args ) throws IOException
	{
		System.out.println( "Hello!" );

		// Set up assistant
		Assistant assistant = null;

		//ListDialogNodesOptions options = new ListDialogNodesOptions.Builder(workspaceId).build();
		//DialogNodeCollection response = assistant.listDialogNodes(options).execute().getResult();
		//System.out.println(response);

		// Get path of csv file
		File jarPath=new File(App.class.getProtectionDomain().getCodeSource().getLocation().getPath());
		String csvPath=jarPath.getParentFile().getAbsolutePath();
		System.out.println("csv file should be placed in: "+csvPath);
		String csvFile = csvPath + "/" + csvName;

		CSVReader reader = null;
		try {
			reader = new CSVReader(new FileReader(csvFile));
			String[] line;
			int linecount = 0;
			while ((line = reader.readNext()) != null) {
				
				// read the first 2 lines to get the account's service credential
				if (linecount == 0) {
					workspaceId = line[0];
					apiKey = line[1];
					serviceUrl = line[2];
					linecount ++;
					continue;
				} else if (linecount == 1) {
					folderNodeId = line[0];
					linecount ++;
					IamAuthenticator authenticator = new IamAuthenticator(apiKey);
					assistant = new Assistant("2019-02-28",  authenticator);
					assistant.setServiceUrl(serviceUrl);
					System.out.println("workspace id: " + workspaceId);
					System.out.println("api key: " + apiKey);
					System.out.println("service url: " + serviceUrl);
					System.out.println("node id: " + folderNodeId);
					continue;
				}
				int count=0;
				String intent = "", answer = "";
				List<String> examples = new ArrayList<String>();
				// go thourgh each column in this row
				for (String cell:line) {
					if (count == 0) {
						// use the first column as the intent and dialog node id
						intent = cell;
					} else if(count == 1) {
						// use the second column as the response 
						answer = cell;
					} else {
						// use the content from the rest of the row as example questions
						if (cell.length() >= 1) {
							examples.add(cell);
						}
					}
					count++;
				}
				try {
					addIntent(assistant, intent, examples);
					addNode(assistant,intent,"#"+intent,answer);
					System.out.println("added: id= " + intent + ", answer= " + answer + "]");
				} catch (Exception e) {
					System.out.println("Failed to add" + intent + ", Stack Trace:");
					e.printStackTrace();
					System.out.println("==========================================");
				}
			}
		}		
		catch (CsvValidationException e) {
			// TODO Auto-generated catch block
			System.out.println("Failed to read csv");
			e.printStackTrace();
		}
		System.out.println("Finished, press enter to exit");
		System.in.read();
	}

	private static void addIntent(Assistant assistant, String intent_name, List<String> userExamples) {
		String intent = intent_name;

		List<Example> examples = new ArrayList<Example>();
		for (String userExample : userExamples) {
			examples.add(new Example.Builder(userExample).build());
		}

		CreateIntentOptions options = new CreateIntentOptions.Builder(workspaceId, intent)
				.examples(examples)
				.build();

		Intent response = assistant.createIntent(options).execute().getResult();

		System.out.println(response);

	}

	public static void addNode(Assistant assistant, String dialogNode, String conditions, String response) {
		DialogNodeOutputTextValuesElement values = new DialogNodeOutputTextValuesElement.Builder().text(response).build();
		DialogNodeOutputGeneric generic = new DialogNodeOutputGeneric.Builder("text").addValues(values).build();
		List<DialogNodeOutputGeneric> genericList = new ArrayList<DialogNodeOutputGeneric>();
		genericList.add(generic);
		DialogNodeOutput output = new DialogNodeOutput();
		output.setGeneric(genericList);
		CreateDialogNodeOptions nodeOptions = new CreateDialogNodeOptions.Builder(workspaceId, dialogNode)
				.conditions(conditions)
				.parent(folderNodeId)
				.output(output)
				.build();

		DialogNode nodeResponse = assistant.createDialogNode(nodeOptions).execute().getResult();

		System.out.println(nodeResponse);
	}
}
