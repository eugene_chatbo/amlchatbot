
**Bulk intent/dialog import script for Windows**

This java program is used to import multiple Q&As to the Watson Assistant Dialog skill at once with a CSV file.

---

## Prepare your csv file

You can add your questions and answer in a csv file then use this script to upload them into your Assistant dialog skill.
You will also need to include the credential of your account in the csv so the script will know which account to upload to.
please make sure the format of the file is correct, else the script will not be able to upload properly

1. The first 3 cell from the first row in order should be: **workspace id**, **api**, **service url**.
2. The first cell from the second row should be the id of the folder that you want the new dialog nodes to be created in.
3. Starting from the third row the first column will be **the intent**, **the response**, **example questions**
4. The intent should only have alphabets or numbers, do not include any special characters except for _ and - (so no space, bracket... etc.) 
5. The response will be the content of a single text response, if you want other types of response or multiple responses, you will need to go to IBM cloud and change it there
6. You can add as many columns of example questions starting from the third columns.
7. Your file need to be named **intent_import.csv** in order to be recognized by the script

---

## Use the script

Run the script with **assistant-import-0.0.1-SNAPSHOT.bat** make sure **assistant-import-0.0.1-SNAPSHOT-shaded.jar** and your **intent_import.csv** is in the same folder with the .bat file.

These files can be found in **assistant-import script** directory

You will need to have Java installed in your computer, you should already have it if you have run any java programs on your computer before. 
Here are some guides on how to do it if you don't have it installed on your computer:

1. For Windows users: https://windowsreport.com/jar-file-windows/
2. For Linux users: https://www.guru99.com/how-to-install-java-on-ubuntu.html
3. For Mac users: https://java.com/en/download/help/mac_install.xml


The .bat file only runs in Windows, you might need to run the jar with terminal/ commandline etc. if you are using linux or mac
